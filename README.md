# Analyze API client


## Install

Via Composer

``` bash
$ composer require analyze/api-client
```

## License

The MIT License (MIT). Please see [License File](LICENSE) for more information.
