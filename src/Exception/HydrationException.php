<?php

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Exception;

use Analyze\ApiClient\Exception;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class HydrationException extends \RuntimeException implements Exception
{
}
