<?php

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Exception;

use Analyze\ApiClient\Exception;

/**
 * @author Márk Sági-Kazár <mark.sagikazar@gmail.com>
 */
interface DomainException extends Exception
{
}
