<?php

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Exception\Domain;

use Analyze\ApiClient\Exception\DomainException;

/**
 * @author Márk Sági-Kazár <mark.sagikazar@gmail.com>
 */
class ValidationException extends BadRequestException implements DomainException
{
    public function __construct($message, $code, array $source)
    {
        $message = sprintf('%s: "%s" - %s', $message, $source['parameter'] ?? '', $source['message'] ?? '');
        parent::__construct($message, $code);
    }
}
