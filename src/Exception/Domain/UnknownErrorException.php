<?php

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Exception\Domain;

use Analyze\ApiClient\Exception\DomainException;

/**
 * @author Márk Sági-Kazár <mark.sagikazar@gmail.com>
 */
class UnknownErrorException extends \RuntimeException implements DomainException
{
}
