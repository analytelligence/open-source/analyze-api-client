<?php

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Exception\Domain;

use Analyze\ApiClient\Exception\DomainException;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class BadRequestException extends \RuntimeException implements DomainException
{
}
