<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Api;

use Analyze\ApiClient\Exception\Domain as DomainExceptions;
use Analyze\ApiClient\Exception\DomainException;
use Analyze\ApiClient\Hydrator\ArrayHydrator;
use Analyze\ApiClient\Hydrator\Hydrator;
use Analyze\ApiClient\Hydrator\NoopHydrator;
use Http\Client\HttpClient;
use Http\Message\RequestFactory;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
abstract class HttpApi
{
    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var Hydrator
     */
    protected $hydrator;

    /**
     * @var RequestFactory
     */
    protected $requestFactory;

    public function __construct(HttpClient $httpClient, Hydrator $hydrator, RequestFactory $requestFactory)
    {
        $this->httpClient = $httpClient;
        $this->requestFactory = $requestFactory;
        if (!$hydrator instanceof NoopHydrator) {
            $this->hydrator = $hydrator;
        }
    }

    /**
     * Send a GET request with query parameters.
     *
     * @param string $path           Request path
     * @param array  $params         GET parameters
     * @param array  $requestHeaders Request Headers
     */
    protected function httpGet(string $path, array $params = [], array $requestHeaders = []): ResponseInterface
    {
        if (count($params) > 0) {
            $path .= '?'.http_build_query($params);
        }

        return $this->httpClient->sendRequest(
            $this->requestFactory->createRequest('GET', $path, $requestHeaders)
        );
    }

    /**
     * Send a POST request with JSON-encoded parameters.
     *
     * @param string $path           Request path
     * @param array  $params         POST parameters to be JSON encoded
     * @param array  $requestHeaders Request headers
     */
    protected function httpPost(string $path, array $params = [], array $requestHeaders = []): ResponseInterface
    {
        if (!isset($requestHeaders['Content-Type'])) {
            $requestHeaders['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        return $this->httpPostRaw($path, $this->createFormBody($params), $requestHeaders);
    }

    /**
     * Send a POST request with raw data.
     *
     * @param string       $path           Request path
     * @param array|string $body           Request body
     * @param array        $requestHeaders Request headers
     */
    protected function httpPostRaw(string $path, $body, array $requestHeaders = []): ResponseInterface
    {
        return $response = $this->httpClient->sendRequest(
            $this->requestFactory->createRequest('POST', $path, $requestHeaders, $body)
        );
    }

    /**
     * Send a DELETE request with JSON-encoded parameters.
     *
     * @param string $path           Request path
     * @param array  $params         POST parameters to be JSON encoded
     * @param array  $requestHeaders Request headers
     */
    protected function httpDelete(string $path, array $params = [], array $requestHeaders = []): ResponseInterface
    {
        return $this->httpClient->sendRequest(
            $this->requestFactory->createRequest('DELETE', $path, $requestHeaders, $this->createFormBody($params))
        );
    }

    /**
     * Handle HTTP errors.
     *
     * Call is controlled by the specific API methods.
     *
     * @throws DomainException
     */
    protected function handleErrors(ResponseInterface $response)
    {
        try {
            $data = (new ArrayHydrator())->hydrate($response, \stdClass::class);
            $error = $data['errors'][0];
        } catch (\Throwable $t) {
            $error['title'] = $t->getMessage();
            $error['status'] = 0;
        }
        switch ($response->getStatusCode()) {
            case 400:
                if (!isset($error['source'])) {
                    throw new DomainExceptions\BadRequestException($error['title'], (int) $error['status']);
                }

                throw new DomainExceptions\ValidationException($error['title'], (int) $error['status'], $error['source'] ?? []);
            case 401:
                throw new DomainExceptions\UnauthorizedException($error['title'], (int) $error['status']);
            case 403:
                throw new DomainExceptions\ForbiddenException($error['title'], (int) $error['status']);
            case 404:
                throw new DomainExceptions\NotFoundException($error['title'], (int) $error['status']);
            default:
                throw new DomainExceptions\UnknownErrorException($error['title'], (int) $error['status']);
        }
    }

    /**
     * Create a JSON encoded version of an array of parameters.
     *
     * @param array $params Request parameters
     *
     * @return null|string
     */
    private function createFormBody(array $params)
    {
        return (0 === count($params)) ? null : http_build_query($params);
    }
}
