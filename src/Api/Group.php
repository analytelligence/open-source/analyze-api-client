<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Api;

use Analyze\ApiClient\Exception;
use Analyze\ApiClient\Exception\InvalidArgumentException;
use Analyze\ApiClient\Model\EntityCreated;
use Analyze\ApiClient\Model\Group as Model;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class Group extends HttpApi
{
    /**
     * @throws Exception
     *
     * @return Model|ResponseInterface
     */
    public function get(string $uuid)
    {
        if (empty($uuid)) {
            throw new InvalidArgumentException('Uuid cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/groups/%s', $uuid));

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }

    public function create()
    {
        $response = $this->httpPost('/api/v1/groups/create');

        if (!$this->hydrator) {
            return $response;
        }

        if (201 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, EntityCreated::class);
    }

    public function addCandidate(string $uuid, string $candidate, int $role)
    {
        $response = $this->httpPost(sprintf('/api/v1/groups/%s/add', $uuid), [
            'candidate' => $candidate,
            'role' => $role,
        ]);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }

    public function removeCandidate(string $uuid, string $candidate)
    {
        $response = $this->httpDelete(sprintf('/api/v1/groups/%s/remove', $uuid), [
            'candidate' => $candidate,
        ]);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }
}
