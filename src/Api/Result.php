<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Api;

use Analyze\ApiClient\Exception;
use Analyze\ApiClient\Exception\InvalidArgumentException;
use Analyze\ApiClient\Model\MeanResult;
use Analyze\ApiClient\Model\Result as Model;
use Analyze\ApiClient\Model\ResultExtended;
use Analyze\ApiClient\Model\ResultReport;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class Result extends HttpApi
{
    /**
     * @param string $test      uuid of the test
     * @param string $candidate uuid of the candidate
     * @param array  $params    {
     *
     *      @var string $norm UUID
     *      @var string $group UUID
     *      @var int $role the role for the candidate in the $group
     * }
     *
     * @throws Exception
     *
     * @return Model|ResponseInterface
     */
    public function get(string $test, string $candidate, array $params = [])
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }
        if (empty($candidate)) {
            throw new InvalidArgumentException('Candidate UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/results/%s', $test, $candidate), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }

    /**
     * @param string $test   uuid of the test
     * @param string $group  uuid of the group
     * @param array  $params {
     *
     *      @var string $norm UUID
     * }
     *
     * @throws Exception
     *
     * @return Model|ResponseInterface
     */
    public function getGroupMean(string $test, string $group, array $params = [])
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }
        if (empty($group)) {
            throw new InvalidArgumentException('Group UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/results/%s/group', $test, $group), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, MeanResult::class);
    }

    /**
     * @param string $test   uuid of the test
     * @param string $group  uuid of the group
     * @param array  $params {
     *
     *      @var string $norm UUID
     *      @var string $compare_group UUID
     *      @var string $compare_test UUID
     * }
     *
     * @throws Exception
     *
     * @return Model|ResponseInterface
     *
     * @deprecated Use getGroupReportV2 instead
     */
    public function getGroupReport(string $test, string $group, array $params = [])
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }
        if (empty($group)) {
            throw new InvalidArgumentException('Group UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/results/%s/group-report', $test, $group), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, ResultReport::class);
    }

    /**
     * @param string $test   uuid of the test
     * @param string $group  uuid of the group
     * @param array  $params {
     *
     *      @var string $compare_group UUID
     *      @var string $name Name for the candidate
     *      @var string $compare_name Name for the compare group
     * }
     *
     * @throws Exception
     *
     * @return Model|ResponseInterface
     */
    public function getGroupReportV2(string $test, string $group, array $params = [])
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }
        if (empty($group)) {
            throw new InvalidArgumentException('Group UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/results/%s/group-report-v2', $test, $group), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, ResultReport::class);
    }

    /**
     * @param string $test      uuid of the test
     * @param string $candidate uuid of the candidate
     * @param array  $params    {
     *
     *      @var string $norm UUID
     *      @var string $group UUID
     *      @var string $compare_group UUID
     *      @var string $compare_test UUID
     *      @var int $role the role for the candidate in the $group
     *      @var bool $regenerate If the report should be regenerated or not
     * }
     *
     * @throws Exception
     *
     * @return ResponseInterface|ResultReport
     *
     * @deprecated Use getReportV2 instead
     */
    public function getReport(string $test, string $candidate, array $params = [])
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }
        if (empty($candidate)) {
            throw new InvalidArgumentException('Candidate UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/results/%s/report', $test, $candidate), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, ResultReport::class);
    }

    /**
     * @param string $test      uuid of the test
     * @param string $candidate uuid of the candidate
     * @param array  $params    {
     *
     *      @var string $compare_group UUID
     *      @var string $name Name for the candidate
     *      @var string $compare_name Name for the compare group
     *      @var bool $regenerate If the report should be regenerated or not
     * }
     *
     * @throws Exception
     *
     * @return ResponseInterface|ResultReport
     */
    public function getReportV2(string $test, string $candidate, array $params = [])
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }
        if (empty($candidate)) {
            throw new InvalidArgumentException('Candidate UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/results/%s/report-v2', $test, $candidate), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, ResultReport::class);
    }

    /**
     * @param string $test      uuid of the test
     * @param string $candidate uuid of the candidate
     * @param array  $params    {
     *
     *      @var string $norm UUID
     *      @var string $group UUID
     *      @var int $role the role for the candidate in the $group
     * }
     *
     * @throws Exception
     *
     * @return ResponseInterface|ResultExtended
     */
    public function getExtended(string $test, string $candidate, array $params = [])
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }
        if (empty($candidate)) {
            throw new InvalidArgumentException('Candidate UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/results/%s/extended', $test, $candidate), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, ResultExtended::class);
    }
}
