<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Api;

use Analyze\ApiClient\Exception;
use Analyze\ApiClient\Exception\InvalidArgumentException;
use Analyze\ApiClient\Model\Norm as Model;
use Analyze\ApiClient\Model\NormCollection;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class Norm extends HttpApi
{
    /**
     * @param string $uuid of the test
     *
     * @throws Exception
     *
     * @return Model|ResponseInterface
     */
    public function index(string $uuid)
    {
        if (empty($uuid)) {
            throw new InvalidArgumentException('Uuid cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/norms', $uuid));

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, NormCollection::class);
    }
}
