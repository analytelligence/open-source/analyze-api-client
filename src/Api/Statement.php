<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Api;

use Analyze\ApiClient\Exception;
use Analyze\ApiClient\Exception\InvalidArgumentException;
use Analyze\ApiClient\Model\StatementCollection as Model;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class Statement extends HttpApi
{
    /**
     * @throws Exception
     *
     * @return null|Model|ResponseInterface
     */
    public function getNext(string $test, string $candidate, array $parameters)
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }

        if (empty($candidate)) {
            throw new InvalidArgumentException('Candidate UUID cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s/candidates/%s/next-statement?%s', $test, $candidate, http_build_query($parameters)));

        if (!$this->hydrator) {
            return $response;
        }

        if (204 === $response->getStatusCode()) {
            return null;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }

    public function answer(string $test, string $statement, string $candidate, array $params)
    {
        if (empty($test)) {
            throw new InvalidArgumentException('Test UUID cannot be empty');
        }

        if (empty($statement)) {
            throw new InvalidArgumentException('Statement UUID cannot be empty');
        }

        if (empty($candidate)) {
            throw new InvalidArgumentException('Candidate UUID cannot be empty');
        }

        if (empty($params)) {
            throw new InvalidArgumentException('Data cannot be empty');
        }

        $params['candidate'] = $candidate;
        $response = $this->httpPost(sprintf('/api/v1/tests/%s/statements/%s', $test, $statement), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (204 === $response->getStatusCode()) {
            return null;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }
}
