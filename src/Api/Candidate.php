<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Api;

use Analyze\ApiClient\Exception;
use Analyze\ApiClient\Exception\InvalidArgumentException;
use Analyze\ApiClient\Model\Candidate as Model;
use Analyze\ApiClient\Model\EntityCreated;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class Candidate extends HttpApi
{
    /**
     * @param string $username
     * @param array  $params
     *
     * @throws Exception
     *
     * @return Model|ResponseInterface
     */
    public function get(string $uuid)
    {
        if (empty($uuid)) {
            throw new InvalidArgumentException('Uuid cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/candidates/%s', $uuid));

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }

    /**
     * @return EntityCreated|ResponseInterface
     */
    public function create()
    {
        $response = $this->httpPost('/api/v1/candidates/create');

        if (!$this->hydrator) {
            return $response;
        }

        if (201 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, EntityCreated::class);
    }
}
