<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient\Api;

use Analyze\ApiClient\Exception;
use Analyze\ApiClient\Exception\InvalidArgumentException;
use Analyze\ApiClient\Model\EntityCreated;
use Analyze\ApiClient\Model\Test\Test as Model;
use Analyze\ApiClient\Model\Test\TestCollection;
use Analyze\ApiClient\Model\Test\TestTemplate;
use Analyze\ApiClient\Model\Test\TestTemplateCollection;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class Test extends HttpApi
{
    /**
     * @param string $uuid
     *
     * @throws Exception
     *
     * @return ResponseInterface|TestCollection
     */
    public function index()
    {
        $response = $this->httpGet('/api/v1/tests');

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, TestCollection::class);
    }

    /**
     * @throws Exception
     *
     * @return Model|ResponseInterface
     */
    public function get(string $uuid)
    {
        if (empty($uuid)) {
            throw new InvalidArgumentException('Uuid cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/%s', $uuid));

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, Model::class);
    }

    /**
     * @param array $params {
     *
     *      @var string $norm uuid
     *      @var array $dimensions array of uuids.
     * }
     *
     * @return EntityCreated|ResponseInterface
     */
    public function create(string $template, array $params = [])
    {
        $params['template'] = $template;
        $response = $this->httpPost('/api/v1/tests/create', $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (201 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, EntityCreated::class);
    }

    /**
     * @param array $params {
     *
     *      @var string $language 2 char language of the dimension names
     * }
     *
     * @return ResponseInterface|TestTemplateCollection
     */
    public function templateIndex(array $params)
    {
        $response = $this->httpGet('/api/v1/tests/templates', $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, TestTemplateCollection::class);
    }

    /**
     * @param array $params {
     *
     *      @var string $language 2 char language of the dimension names
     * }
     *
     * @throws Exception
     *
     * @return ResponseInterface|TestTemplate
     */
    public function getTemplate(string $uuid, array $params)
    {
        if (empty($uuid)) {
            throw new InvalidArgumentException('Uuid cannot be empty');
        }

        $response = $this->httpGet(sprintf('/api/v1/tests/templates/%s', $uuid), $params);

        if (!$this->hydrator) {
            return $response;
        }

        if (200 !== $response->getStatusCode()) {
            $this->handleErrors($response);
        }

        return $this->hydrator->hydrate($response, TestTemplate::class);
    }
}
