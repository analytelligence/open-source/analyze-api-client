<?php

declare(strict_types=1);

/*
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Analyze\ApiClient;

use Analyze\ApiClient\Hydrator\Hydrator;
use Analyze\ApiClient\Hydrator\ModelHydrator;
use Http\Client\HttpClient;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Message\RequestFactory;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class ApiClient
{
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var Hydrator
     */
    private $hydrator;

    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * The constructor accepts already configured HTTP clients.
     * Use the configure method to pass a configuration to the Client and create an HTTP Client.
     */
    public function __construct(
        HttpClient $httpClient,
        Hydrator $hydrator = null,
        RequestFactory $requestFactory = null
    ) {
        $this->httpClient = $httpClient;
        $this->hydrator = $hydrator ?: new ModelHydrator();
        $this->requestFactory = $requestFactory ?: MessageFactoryDiscovery::find();
    }

    /**
     * @return ApiClient
     */
    public static function configure(
        HttpClientConfigurator $httpClientConfigurator,
        Hydrator $hydrator = null,
        RequestFactory $requestFactory = null
    ): self {
        $httpClient = $httpClientConfigurator->createConfiguredClient();

        return new self($httpClient, $hydrator, $requestFactory);
    }

    public static function create(string $username, string $apiKey): ApiClient
    {
        $httpClientConfigurator = (new HttpClientConfigurator())->setApiKey($username, $apiKey);

        return self::configure($httpClientConfigurator);
    }

    public function candidates(): Api\Candidate
    {
        return new Api\Candidate($this->httpClient, $this->hydrator, $this->requestFactory);
    }

    public function groups(): Api\Group
    {
        return new Api\Group($this->httpClient, $this->hydrator, $this->requestFactory);
    }

    public function norms(): Api\Norm
    {
        return new Api\Norm($this->httpClient, $this->hydrator, $this->requestFactory);
    }

    public function results(): Api\Result
    {
        return new Api\Result($this->httpClient, $this->hydrator, $this->requestFactory);
    }

    public function statements(): Api\Statement
    {
        return new Api\Statement($this->httpClient, $this->hydrator, $this->requestFactory);
    }

    public function tests(): Api\Test
    {
        return new Api\Test($this->httpClient, $this->hydrator, $this->requestFactory);
    }
}
