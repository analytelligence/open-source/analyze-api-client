<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class ResultReport implements CreatableFromArray
{
    private $url;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }

        $model->url = $data['url'];

        return $model;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
