<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class Result implements CreatableFromArray
{
    private $uuid;

    private $dimensions = [];

    /**
     * @var int
     */
    private $score;

    /**
     * @var bool
     */
    private $complete;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }

        $model->uuid = $data['uuid'];
        $model->score = $data['score'];
        $model->complete = $data['complete'];
        $model->dimensions = $data['dimensions'] ?? [];

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return array ['code' => ['uuid'=>'string', 'score'=>'int']
     */
    public function getDimensions(): array
    {
        return $this->dimensions;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function isComplete(): bool
    {
        return $this->complete;
    }
}
