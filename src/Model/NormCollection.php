<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class NormCollection implements CreatableFromArray
{
    /**
     * @var Norm[]
     */
    private $norms = [];

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();

        foreach ($data['data'] as $arr) {
            $model->norms[] = Norm::createFromArray($arr);
        }

        return $model;
    }

    /**
     * @return Norm[]
     */
    public function getNorms(): array
    {
        return $this->norms;
    }
}
