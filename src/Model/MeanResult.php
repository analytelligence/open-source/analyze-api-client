<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class MeanResult implements CreatableFromArray
{
    private $dimensions = [];
    private $score;
    private $count;
    private $norm;
    private $group;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }

        $model->group = $data['group'];
        $model->norm = $data['norm'];
        $model->score = (int) $data['score'];
        $model->count = (int) $data['count'];
        $model->dimensions = $data['dimensions'];

        return $model;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getNorm(): string
    {
        return $this->norm;
    }

    /**
     * @return array ['code' => ['uuid'=>'string', 'score'=>'int']
     */
    public function getDimensions(): array
    {
        return $this->dimensions;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function getGroup(): string
    {
        return $this->group;
    }
}
