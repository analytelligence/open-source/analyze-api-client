<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class StatementCollection implements CreatableFromArray
{
    private $uuid;

    private $statements = [];

    private $meta;

    private $imageReplacements;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }

        $model->uuid = $data['uuid'];
        $model->meta = $data['meta'] ?? [];
        $model->imageReplacements = $data['image_replacements'] ?? [];
        foreach ($data['statements'] as $order => $statement) {
            $model->statements[$order] = Statement::createFromArray($statement, $order);
        }

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return Statement[]
     */
    public function getStatements(): array
    {
        return $this->statements;
    }

    /**
     * Get a statement by its uuid.
     *
     * @param mixed $uuid
     *
     * @return null|Statement
     */
    public function getStatement($uuid)
    {
        foreach ($this->statements as $statement) {
            if ($statement->getUuid() === $uuid) {
                return $statement;
            }
        }
    }

    /**
     * @return null|Statement
     */
    public function getFirst()
    {
        foreach ($this->statements as $statement) {
            return $statement;
        }

        return null;
    }

    public function getMeta(): array
    {
        return $this->meta;
    }

    public function getImageReplacements(): array
    {
        return $this->imageReplacements;
    }

    public function getTotalStatements()
    {
        return $this->meta['total_statements'];
    }

    public function getAnsweredStatements()
    {
        return $this->meta['answered_statements'];
    }

    public function getLanguage()
    {
        return $this->meta['language'];
    }

    /**
     * @return int [1-100]
     */
    public function getProgress(): int
    {
        return (int) round(100 * $this->getAnsweredStatements() / $this->getTotalStatements());
    }
}
