<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class Statement implements CreatableFromArray
{
    const TYPE_TEXT = 0;
    const TYPE_IMAGE = 1;
    const TYPE_RANGE = 2;

    private $uuid;
    private $content;
    private $type;
    private $answerType;
    private $order;

    /**
     * @var array
     */
    private $answers;

    private function __construct()
    {
    }

    public static function createFromArray(array $data, $order = null)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }

        $model->uuid = $data['uuid'];
        $model->content = $data['content'];
        $model->type = $data['type'];
        $model->answerType = $data['answer_type'];
        $model->answers = $data['answers'];
        $model->order = $order;

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getAnswerType(): int
    {
        return $this->answerType;
    }

    public function getAnswers(): array
    {
        return $this->answers;
    }

    public function getOrder(): int
    {
        return $this->order;
    }
}
