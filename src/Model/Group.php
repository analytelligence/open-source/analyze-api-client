<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class Group implements CreatableFromArray
{
    private $uuid;

    /**
     * @var string[]
     */
    private $members;

    /**
     * @var string[]
     */
    private $managers;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }
        $model->uuid = $data['uuid'];
        $model->members = $data['members'];
        $model->managers = $data['managers'];

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string[]
     */
    public function getMembers(): array
    {
        return $this->members;
    }

    /**
     * @return string[]
     */
    public function getManagers(): array
    {
        return $this->managers;
    }
}
