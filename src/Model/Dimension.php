<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class Dimension implements CreatableFromArray
{
    private $uuid;
    private $name;
    private $code;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }

        $model->uuid = $data['uuid'];
        $model->name = $data['name'] ?? '';
        $model->code = $data['code'] ?? '';

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
