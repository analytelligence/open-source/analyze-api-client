<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model\Test;

use Analyze\ApiClient\Model\CreatableFromArray;
use Analyze\ApiClient\Model\Dimension;

class Test implements CreatableFromArray
{
    private $uuid;
    private $template;
    private $name;
    private $norm;

    /**
     * @var array
     */
    private $languages;

    /**
     * @var Dimension[]
     */
    private $dimensions;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }
        $model->uuid = $data['uuid'];
        $model->template = $data['template'];
        $model->name = $data['name'];
        $model->norm = $data['norm'];
        $model->languages = $data['languages'];
        $model->dimensions = [];

        foreach ($data['dimensions']['data'] as $dimension) {
            $model->dimensions[] = Dimension::createFromArray($dimension);
        }

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNorm(): string
    {
        return $this->norm;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @return Dimension[]
     */
    public function getDimensions(): array
    {
        return $this->dimensions;
    }
}
