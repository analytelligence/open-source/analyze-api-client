<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model\Test;

use Analyze\ApiClient\Model\CreatableFromArray;

class TestCollection implements CreatableFromArray
{
    /**
     * @var Test[]
     */
    private $tests = [];

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();

        foreach ($data['data'] as $arr) {
            $model->tests[] = Test::createFromArray($arr);
        }

        return $model;
    }

    /**
     * @return Test[]
     */
    public function getTests(): array
    {
        return $this->tests;
    }
}
