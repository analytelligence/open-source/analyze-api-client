<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model\Test;

use Analyze\ApiClient\Model\CreatableFromArray;

class TestTemplate implements CreatableFromArray
{
    private $uuid;
    private $name;
    private $languages = [];
    private $norms = [];
    private $dimensions = [];
    private $availableDimensions = [];

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        if (isset($data['data'])) {
            $data = $data['data'];
        }
        $model = new self();
        $model->uuid = $data['uuid'];
        $model->name = $data['name'];
        $model->languages = $data['languages'];
        $model->dimensions = $data['dimensions'];
        $model->availableDimensions = $data['available_dimensions'];
        $model->norms = $data['norms'];

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function getNorms(): array
    {
        return $this->norms;
    }

    /**
     * @deprecated use getAvailableDimensions instead
     */
    public function getDimensions(): array
    {
        return $this->dimensions;
    }

    /**
     * Return an array with dimension ids as keys.
     *
     * @return array<string, array{name: string, code: string}>
     */
    public function getAvailableDimensions(): array
    {
        return $this->availableDimensions;
    }
}
