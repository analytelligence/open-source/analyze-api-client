<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model\Test;

use Analyze\ApiClient\Model\CreatableFromArray;

class TestTemplateCollection implements CreatableFromArray
{
    /**
     * @var TestTemplate[]
     */
    private $templates = [];

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();

        foreach ($data['data'] as $arr) {
            $model->templates[] = TestTemplate::createFromArray($arr);
        }

        return $model;
    }

    /**
     * @return TestTemplate[]
     */
    public function getTemplates(): array
    {
        return $this->templates;
    }
}
