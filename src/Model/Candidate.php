<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class Candidate implements CreatableFromArray
{
    private $uuid;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }
        $model->uuid = $data['uuid'];

        return $model;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }
}
