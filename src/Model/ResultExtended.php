<?php

declare(strict_types=1);

namespace Analyze\ApiClient\Model;

class ResultExtended implements CreatableFromArray
{
    private $dimensions;

    private function __construct()
    {
    }

    public static function createFromArray(array $data)
    {
        $model = new self();
        if (isset($data['data'])) {
            $data = $data['data'];
        }

        $model->dimensions = $data['dimensions'] ?? [];

        return $model;
    }

    public function getDimensions()
    {
        return $this->dimensions;
    }
}
