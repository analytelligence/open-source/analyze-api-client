# Change Log

## 1.4.0

- Adding support for PHP 8
- Dropped support for PHP <7.4

## 1.3.0

- Adding support for Report V2

## 1.2.0

- Adding support for Group report

## Unreleased

- Initial release
